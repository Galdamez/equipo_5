<!DOCTYPE html>
<html>
<head>
	<title>Competencias</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('props/bootstrap/css/bootstrap.min.css') ?>">
</head>
<body>
<div class="container">
	<h1>Nueva Competencia</h1>
	<br>
	<form action="<?= base_url('CompetenciasController/ingresar') ?>" method="POST" autocomplete="off">
		<div class="row">
			<div class="col">
				<label>Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control">
			</div>
			<div class="col">
				<label>Descripcion</label>
				<input type="text" name="descripcion" id="descripcion" class="form-control">
			</div>
		</div>
		<br>
		<div>
			<input type="submit" value="Guardar" class="btn btn-success">
		</div>
	</form>
	<br>
	<br>
	<table class="table table-hover table-bordered table-striped table-sm" border="1" align="center">
		<thead class="thead-dark">
			<tr>
				<th>N°</th>
				<th>Nombre</th>
				<th>Competencia</th>
			</tr>
		</thead>
		<tbody>
			<?php $n=1; foreach($competencias as $c){ ?>
			<tr>
				<td><?= $n; ?></td>
				<td><?= $c->nombre ?></td>
				<td><?= $c->descripcion ?></td>
			</tr>
			<?php $n++;} ?>
		</tbody>
	</table>
</div>
</body>
</html>