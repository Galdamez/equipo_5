<?php
defined('BASEPATH') or exit('No es permitido accesar');

class CompetenciasDao extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Competencias');
	}

	/*Gestion*/
	/*Fn: 
	@param:
	@return:na
	*/
	public function create($competencias){

		$u = new competencias();
		
		$this->db->set('nombre',$u->getNombre());
		$this->db->set('descripcion',$u->getDescripcion());
		$this->db->insert('competencias');


	}

	/*Fn: Devuelve un listado de clientes
	@param:na
	@return: Lista de clientes
	*/
	public function read(){

		return $this->db->get('competencias')->result();
		
	}

}