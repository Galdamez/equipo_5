<?php

class usuario{

	private $nombre;
	private $apellido;
	private $nomUsuario;
	private $competencias;

	public function getNombre(){

		return $this->nombre;
	}

	public function setNombre($nombre){

		$this->nombre = $nombre;
	}

	public function getApellido(){

		return $this->apellido;
	}

	public function setApellido($apellido){

		$this->apellido = $apellido;
	}

	public function getUsuario(){

		return $this->nomUsuario;
	}

	public function setUsuario($nomUsuario){

		$this->nomUsuario = $nomUsuario;
	}

	public function getCompetencias(){

		return $this->competencias;
	}

	public function setCompetencias($competencias){

		$this->competencias = $competencias;
	}
}