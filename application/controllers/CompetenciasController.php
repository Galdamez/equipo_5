<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CompetenciasController extends CI_Controller {

public function __construct(){

	parent:: __construct();

	$this->load->model('CompetenciasDao');
	$this->load->library('Competencias');
}	

	public function index()
	{
		$data = array('competencias' => $this->CompetenciasDao->read());
		$this->load->view('CompetenciasView',$data);
	}

	public function ingresar(){

		$usu = new competencias();
		$usu->setNombre($this->input->post('nombre'));
		$usu->setDescripcion($this->input->post('descripcion'));

		$res = $this->CompetenciasDao->create($usu);
		redirect('/CompetenciasController/index','refresh');
	}

}
