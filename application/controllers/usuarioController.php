<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuarioController extends CI_Controller {

public function __construct(){

	parent:: __construct();

	$this->load->model('usuarioDao');
	/*$this->load->library('usuario');*/
}	

	public function index()
	{
		$data = array('usuario' => $this->usuarioDao->read());
		$this->load->view('usuarioView',$data);
	}

}
